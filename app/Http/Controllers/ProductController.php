<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Merchant;
use Illuminate\Http\Request;
use App\Models\ProductCategory;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::all();
        return view('dashboard.products.index', compact('products'));
    }

    public function create()
    {
        $merchants = Merchant::all();
        $productCategories = ProductCategory::all();
        return view('dashboard.products.create', compact('merchants', 'productCategories'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'merchant_id' => 'required',
            'product_category_id' => 'required',
            'name' => 'required',
            'price' => 'required|numeric',
            'quantity' => 'required|integer',
            'available' => 'boolean',

        ]);

        $product = Product::create($request->all());

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $path = $image->store('public/images/products');
            $imageUrl = Storage::url($path);
            $product->image = $imageUrl;
            $product->save();
        }

        return redirect()->route('products.index')
            ->with('success', 'Product created successfully.');
    }

    public function show(Product $product)
    {
        return view('dashboard.products.show', compact('product'));
    }

    public function edit(Product $product)
    {
        $merchants = Merchant::all();
        $productCategories = ProductCategory::all();
        return view('dashboard.products.edit', compact('product', 'merchants', 'productCategories'));
    }

    public function update(Request $request, Product $product)
    {
        $request->validate([
            'merchant_id' => 'required',
            'product_category_id' => 'required',
            'name' => 'required',
            'price' => 'required|numeric',
            'quantity' => 'required|integer',
            'available' => 'boolean',
        ]);

        $product->update($request->all());

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $path = $image->store('public/images/products');

            if ($product->image) {
                $imageName = basename($product->image);
                Storage::delete('public/images/products/' . $imageName);
            }

            $imageUrl = Storage::url($path);
            $product->image = $imageUrl;
            $product->save();
        }

        return redirect()->route('products.index')
            ->with('success', 'Product updated successfully.');
    }

    public function destroy(Product $product)
    {
        $product->delete();

        return redirect()->route('products.index')
            ->with('success', 'Product deleted successfully.');
    }
}
