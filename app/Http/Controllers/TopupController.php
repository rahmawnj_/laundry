<?php

namespace App\Http\Controllers;

use Midtrans\Snap;
use Midtrans\Config;
use App\Models\Topup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;

class TopupController extends Controller
{
    public function showTopupForm()
    {
        return view('topup');
    }

    public function processTopup(Request $request)
    {
        // Lakukan validasi input
        $validatedData = $request->validate([
            'amount' => 'required|numeric|min:1',
        ]);
        $topup = Topup::create([
            'user_id' => 2,
            'amount' => $validatedData['amount'],
            'payment_method' => 'transfer', // Ganti dengan metode pembayaran yang sesuai
            'transaction_date' => now(),
        ]);
        // Set konfigurasi Midtrans
        \Midtrans\Config::$serverKey = 'SB-Mid-server-uD1WCOjCwkFyhQ74qRGA-8ak';
        \Midtrans\Config::$clientKey = 'SB-Mid-client-tG-FKW7GgcMzVvHJ';
        \Midtrans\Config::$isProduction = false;

        $transactionDetails = [
            'transaction_details' => [
                'order_id' => "T" . $topup->id,
                'gross_amount' => (float) $validatedData['amount']
            ]
        ];

        // $snapToken = \Midtrans\Snap::getSnapToken($transactionDetails);
        $redirectUrl = \Midtrans\Snap::createTransaction($transactionDetails)->redirect_url;

        return redirect()->away($redirectUrl);
    }


    public function notificationHandler(Request $request)
    {
        // Verifikasi signature dari Midtrans
        $orderId = $request->query('order_id');
        $statusCode = $request->query('status_code');
        $transactionStatus = $request->query('transaction_status');

        // Proses sesuai dengan status transaksi
        if ($transactionStatus == 'capture') {
        } elseif ($transactionStatus == 'settlement') {
            $topup = Topup::where('id', 1)->first();
            $user = User::find($topup->user_id);
            $user->balance->amount += $topup->amount;
            $user->balance->save();
        } elseif ($transactionStatus == 'pending') {
            // Pembayaran top-up masih dalam proses
        } elseif ($transactionStatus == 'deny') {
            // Pembayaran top-up ditolak
        } elseif ($transactionStatus == 'expire') {
            // Pembayaran top-up kadaluarsa
        } elseif ($transactionStatus == 'cancel') {
            // Pembayaran top-up dibatalkan
        }

        // Kirim response OK ke Midtrans
        return response('OK');
    }
}
