<?php

namespace App\Http\Controllers;

use App\Models\Balance;
use App\Models\Employee;
use App\Models\Position;
use App\Models\User;
use App\Models\OperationalTime;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function index()
    {
        $employees = Employee::all();
        return view('dashboard.employees.index', compact('employees'));
    }

    public function create()
    {
        $positions = Position::all();
        $users = User::all();
        $operationalTimes = OperationalTime::all();
        return view('dashboard.employees.create', compact('positions', 'users', 'operationalTimes'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'rfid' => 'nullable|unique:employees',
            'position_id' => 'required|exists:positions,id',
            'user_id' => 'required|exists:users,id',
            'operational_time_id' => 'required|exists:operational_times,id',
        ]);

        Employee::create($request->all());

        Balance::create([
            'user_id' => $request->user_id
        ]);

        return redirect()->route('employees.index')->with('success', 'Employee created successfully.');
    }

    public function show(Employee $employee)
    {
        return view('dashboard.employees.show', compact('employee'));
    }

    public function edit(Employee $employee)
    {
        $positions = Position::all();
        $users = User::all();
        $operationalTimes = OperationalTime::all();
        return view('dashboard.employees.edit', compact('employee', 'positions', 'users', 'operationalTimes'));
    }

    public function update(Request $request, Employee $employee)
    {
        $request->validate([
            'name' => 'required',
            'rfid' => 'nullable|unique:employees,rfid,' . $employee->id,
            'position_id' => 'required|exists:positions,id',
            'user_id' => 'required|exists:users,id',
            'operational_time_id' => 'required|exists:operational_times,id',
        ]);

        $employee->update($request->all());

        return redirect()->route('employees.index')->with('success', 'Employee updated successfully.');
    }

    public function destroy(Employee $employee)
    {
        $employee->delete();

        return redirect()->route('employees.index')->with('success', 'Employee deleted successfully.');
    }
}
