<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Balance;
use App\Models\Student;
use App\Models\Classroom;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StudentController extends Controller
{
    public function index()
    {
        $students = Student::all();
        return view('dashboard.students.index', compact('students'));
    }

    public function create()
    {
        $classrooms = Classroom::all();
        $users = User::all();
        return view('dashboard.students.create', compact('users', 'classrooms'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'rfid' => 'nullable|unique:students',
            'classroom_id' => 'required|exists:classrooms,id',
            'user_id' => 'required|exists:users,id',
        ]);

        Student::create($request->all());

        Balance::create([
            'user_id' => $request->user_id
        ]);

        return redirect()->route('students.index')->with('success', 'Student created successfully.');
    }

    public function show(Student $student)
    {
        return view('dashboard.students.show', compact('student'));
    }

    public function edit(Student $student)
    {
        $users = User::all();

        $classrooms = Classroom::all();
        return view('dashboard.students.edit', compact('student', 'users', 'classrooms'));
    }

    public function update(Request $request, Student $student)
    {
        $request->validate([
            'name' => 'required',
            'rfid' => 'nullable|unique:students,rfid,' . $student->id,
            'classroom_id' => 'required|exists:classrooms,id',
            'user_id' => 'required|exists:users,id',
        ]);

        $student->update($request->all());

        return redirect()->route('students.index')->with('success', 'Student updated successfully.');
    }

    public function destroy(Student $student)
    {
        $student->delete();

        return redirect()->route('students.index')->with('success', 'Student deleted successfully.');
    }
}
