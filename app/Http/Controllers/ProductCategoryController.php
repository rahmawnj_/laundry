<?php

namespace App\Http\Controllers;

use App\Models\productcategory;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class productcategoryController extends Controller
{
    public function index()
    {
        $productcategories = productcategory::all();
        return view('dashboard.productcategories.index', compact('productcategories'));
    }

    public function create()
    {
        return view('dashboard.productcategories.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'image' => 'string',
        ]);

        productcategory::create([
            'name' => $request->name,
            'icon' => $request->icon,
        ]);

        return redirect()->route('productcategories.index')->with('success', 'productcategory created successfully');
    }

    public function edit(productcategory $productcategory)
    {
        return view('dashboard.productcategories.edit', compact('productcategory'));
    }

    public function update(Request $request, productcategory $productcategory)
    {
        $request->validate([
            'name' => 'required',
            'icon' => 'string',
        ]);

        $productcategory->update([
            'name' => $request->name,
            'icon' => $request->icon,
        ]);
        return redirect()->route('productcategories.index')->with('success', 'productcategory updated successfully');
    }

    public function destroy(productcategory $productcategory)
    {
        $productcategory->delete();
        return redirect()->route('productcategories.index')->with('success', 'productcategory deleted successfully');
    }
}
