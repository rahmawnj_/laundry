<?php

namespace App\Http\Controllers;

use App\Models\Owner;
use App\Models\Outlet;
use App\Models\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class TransactionController extends Controller
{
  public function addTransaction(Request $request)
{
    $token = $request->input('token');

    $outlet = Outlet::where('api_token', $token)->first();

    if (!$outlet) {
        return response()->json(['error' => 'Invalid token'], 401);
    }

    try {
        $transactionData = [
            'outlet_id' => $outlet->id,
            'amount' => $request->input('amount'),
            'rfid' => $request->input('rfid'),
            'status' => $request->input('status'),
            'time' => now(),
        ];

        Transaction::create($transactionData);

        return response()->json(['message' => 'Transaction saved successfully', 'status' => 'success']);
    } catch (\Exception $e) {
        return response()->json(['error' => 'Failed to save transaction', 'status' => 'error', 'details' => $e->getMessage()], 500);
    }
}


public function index(Request $request)
{
    $user = Auth::user();

    if ($user->hasRole('admin')) {
        $outlets = Outlet::all();
    } elseif ($user->hasRole('owner')) {
        $outlets = $user->outlets ?? collect();
    }

    $selectedOutletId = $request->get('outlet_id');
    $startDate = $request->get('start_date');
    $endDate = $request->get('end_date');

    $transactions = Transaction::when($selectedOutletId, function ($query) use ($selectedOutletId) {
        $query->where('outlet_id', $selectedOutletId);
    })
    ->when($startDate, function ($query) use ($startDate) {
        $query->where('time', '>=', Carbon::parse($startDate)->startOfDay());
    })
    ->when($endDate, function ($query) use ($endDate) {
        $query->where('time', '<=', Carbon::parse($endDate)->endOfDay());
    })
    ->orderBy('time', 'desc') // Menambahkan orderBy untuk mengurutkan berdasarkan waktu (descending)
    ->get();

    return view('dashboard.transactions.index', [
        'transactions' => $transactions,
        'outlets' => $outlets,
    ]);
}


}
