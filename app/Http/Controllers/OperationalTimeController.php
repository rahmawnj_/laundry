<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\OperationalTime;

class OperationalTimeController extends Controller
{
    public function index()
    {
        $operationalTimes = OperationalTime::all();
        return view('dashboard.operational_times.index', compact('operationalTimes'));
    }

    public function create()
    {
        return view('dashboard.operational_times.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'check_in_start' => 'required',
            'check_in_end' => 'required',
            'check_out_start' => 'required',
            'check_out_end' => 'required',
            'late_time' => 'required',
            'label' => 'required',
        ]);

        OperationalTime::create($validatedData);
        return redirect()->route('operational_times.index')->with('success', 'Operational time created successfully.');
    }

    public function edit(OperationalTime $operationalTime)
    {
        return view('dashboard.operational_times.edit', compact('operationalTime'));
    }

    public function update(Request $request, OperationalTime $operationalTime)
    {
        $validatedData = $request->validate([
            'check_in_start' => 'required',
            'check_in_end' => 'required',
            'check_out_start' => 'required',
            'check_out_end' => 'required',
            'late_time' => 'required',
            'label' => 'required',
        ]);

        $operationalTime->update($validatedData);
        return redirect()->route('operational_times.index')->with('success', 'Operational time updated successfully.');
    }

    public function destroy(OperationalTime $operationalTime)
    {
        $operationalTime->delete();
        return redirect()->route('operational_times.index')->with('success', 'Operational time deleted successfully.');
    }
}
