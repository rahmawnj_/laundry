<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Balance;
use App\Models\Merchant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MerchantController extends Controller
{
    public function index()
    {
        $merchants = Merchant::all();
        return view('dashboard.merchants.index', compact('merchants'));
    }

    public function create()
    {
        $users = User::all();

        return view('dashboard.merchants.create', compact('users'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'address' => 'required',
            'contact' => 'required',
            'user_id' => 'required',
        ]);

        Merchant::create($request->all());

        Balance::create([
            'user_id' => $request->user_id
        ]);

        return redirect()->route('merchants.index')
            ->with('success', 'Merchant created successfully.');
    }

    public function show(Merchant $merchant)
    {
        return view('dashboard.merchants.show', compact('merchant'));
    }

    public function edit(Merchant $merchant)
    {
        $users = User::all();

    return view('dashboard.merchants.edit', compact('merchant', 'users'));
    }

    public function update(Request $request, Merchant $merchant)
    {
        $request->validate([
            'name' => 'required',
            'address' => 'required',
            'contact' => 'required',
            'user_id' => 'required',
        ]);

        $merchant->update($request->all());

        return redirect()->route('merchants.index')
            ->with('success', 'Merchant updated successfully.');
    }

    public function destroy(Merchant $merchant)
    {
        $merchant->delete();

        return redirect()->route('merchants.index')
            ->with('success', 'Merchant deleted successfully.');
    }
}
