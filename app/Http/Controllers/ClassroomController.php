<?php

namespace App\Http\Controllers;

use App\Models\Classroom;
use App\Models\OperationalTime;
use Illuminate\Http\Request;

class ClassroomController extends Controller
{
    public function index()
    {
        $classrooms = Classroom::with('operationalTime')->get();

        return view('dashboard.classrooms.index', compact('classrooms'));
    }

    public function create()
    {
        $operationalTimes = OperationalTime::all();
        return view('dashboard.classrooms.create', compact('operationalTimes'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        Classroom::create($request->all());

        return redirect()->route('classrooms.index')->with('success', 'Classroom created successfully.');
    }

    public function edit(Classroom $classroom)
    {
        $operationalTimes = OperationalTime::all();
        return view('dashboard.classrooms.edit', compact('classroom', 'operationalTimes'));
    }

    public function update(Request $request, Classroom $classroom)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $classroom->update($request->all());

        return redirect()->route('classrooms.index')->with('success', 'Classroom updated successfully.');
    }

    public function destroy(Classroom $classroom)
    {
        $classroom->delete();

        return redirect()->route('classrooms.index')->with('success', 'Classroom deleted successfully.');
    }
}
