<?php

namespace App\Http\Controllers;

use App\Models\Outlet;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;


class OutletController extends Controller
{

    public function index()
    {
        // Mendapatkan pemilik (owner) yang sedang login
        $owner = Auth::user();

        // Jika pemilik (owner) memiliki role 'owner', ambil outlet yang dimilikinya
        if ($owner->hasRole('owner')) {
            $outlets = $owner->outlets;
        } else {
            // Jika bukan pemilik, ambil semua outlet
            $outlets = Outlet::all();
        }
        return view('dashboard.outlets.index', compact('outlets'));
    }

    public function create()
    {
        return view('dashboard.outlets.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'city' => 'required',
            'code' => 'required|unique:outlets,code,NULL,id,city,' . $request->city,
            'username' => 'required|unique:outlets,username,NULL,id',
            'password' => 'required',
            'logo' => 'max:2048', // Adjust the validation rules for the logo
        ]);


        $data = $request->all();
        $data['password'] = bcrypt($request->input('password'));


        // if ($request->hasFile('logo')) {
        //     $logoPath = $request->file('logo')->store('images/outlet', 'public');
        //     $data['logo'] = $logoPath;
        // }

        $owner = Outlet::create($data);

        if ($request->hasFile('logo')) {
            $logo = $request->file('logo');
            $path = $logo->store('public/images/outlets');
            $logoUrl = Storage::url($path);
            $owner->logo = $logoUrl;
            $owner->save();
        }


        return redirect()->route('outlets.index')
            ->with('success', 'Outlet created successfully');
    }


    public function show(Outlet $outlet)
    {
        return view('dashboard.outlets.show', compact('outlet'));
    }

    public function edit(Outlet $outlet)
    {
        return view('dashboard.outlets.edit', compact('outlet'));
    }

    public function update(Request $request, Outlet $outlet)
    {
        $request->validate([
            'name' => 'required',
            'city' => 'required',
            'code' => 'required|unique:outlets,code,' . $outlet->id . ',id,city,' . $request->city,
            'username' => 'required|unique:outlets,username,' . $outlet->id,
            'password' => 'required',
            'logo' => 'max:2048', // Adjust the validation rules for the logo
        ]);

        $data = $request->all();
        $data['password'] = bcrypt($request->input('password'));
        $outlet->update($data);

        if ($request->hasFile('logo')) {
            $logo = $request->file('logo');
            $path = $logo->store('public/images/users');

            if ($outlet->logo) {
                $logoName = basename($outlet->logo);
                Storage::delete('public/images/outlets/' . $logoName);
            }

            // Dapatkan URL gambar menggunakan Storage::url()
            $logoUrl = Storage::url($path);

            $outlet->logo = $logoUrl;
            $outlet->save();
        }


        return redirect()->route('outlets.index')
            ->with('success', 'Outlet updated successfully');
    }


    public function destroy(Outlet $outlet)
    {
        $outlet->delete();

        return redirect()->route('outlets.index')
            ->with('success', 'Outlet deleted successfully');
    }

    public function login(Request $request)
    {
        // Validate the request
        $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

        // Find the outlet by username
        $outlet = Outlet::where('username', $request->input('username'))->first();


        // Check if the outlet exists and the password is correct
        if ($outlet && password_verify($request->input('password'), $outlet->password)) {


            $outlet->api_token = Str::random(60); // Use Str::random
            $outlet->save();

            return response()->json(['outlet' => $outlet]);
        }

        return response()->json(['error' => 'Unauthorized'], 401);
    }

    public function checkToken($token)
    {
        $outlet = Outlet::where('api_token', $token)->first();

        if ($outlet) {
            return response()->json(['status' => 'success']);
        }

        return response()->json(['status' => 'error'], 401);
    }
}
