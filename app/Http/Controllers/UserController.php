<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function index()
    {
        $users = User::whereHas('roles', function ($query) {
            $query->where('name', 'admin');
        })->get();
        return view('dashboard.users.index', compact('users'));
    }

    public function create()
    {
        $roles = Role::all();
        return view('dashboard.users.create', compact('roles'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ])->assignRole('admin');

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $path = $image->store('public/images/users');
            $imageUrl = Storage::url($path);
            $user->image = $imageUrl;
            $user->save();
        }

        return redirect()->route('users.index')->with('success', 'User created successfully');
    }

    public function edit(User $user)
    {
        $roles = Role::all();
        return view('dashboard.users.edit', compact('user', 'roles'));
    }

    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $user->id,
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        $user->update([
            'name' => $request->name,
            'email' => $request->email,
        ]);


        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $path = $image->store('public/images/users');

            if ($user->image) {
                $imageName = basename($user->image);
                Storage::delete('public/images/users/' . $imageName);
            }

            // Dapatkan URL gambar menggunakan Storage::url()
            $imageUrl = Storage::url($path);

            $user->image = $imageUrl;
            $user->save();
        }


        return redirect()->route('users.index')->with('success', 'User updated successfully');
    }

    public function destroy(User $user)
    {
        if ($user->image) {
            $imageName = basename($user->image);
            Storage::delete('public/images/users/' . $imageName);
        }

        $user->delete();

        return redirect()->route('users.index')->with('success', 'User deleted successfully');
    }

    public function form()
    {
        return view('auth.profile');
    }

    public function submit(Request $request)
    {
        $user = Auth::user();

        if (auth()->user()->hasRole('owner')) {
            $user->owner->update([
                'address' => $request->address,
            ]);
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = $request->password ? Hash::make($request->password) : $user->password;

        // Handle image update
        if ($request->hasFile('image')) {
            // Delete the old image if it exists
            if ($user->image) {
                $imageName = basename($user->image);
                Storage::delete('public/images/users/' . $imageName);
            }


            // Upload the new image
            $image = $request->file('image');
            $path = $image->store('public/images/users');
            $imageUrl = Storage::url($path);
            $user->image = $imageUrl;
        }
        $user->save();
        return redirect()->back()->with('success', 'Profile updated successfully');
    }
}
