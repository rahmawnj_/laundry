<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Owner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class OwnerController extends Controller
{
    public function index()
    {
        $owners = Owner::all();
        return view('dashboard.owners.index', compact('owners'));
    }

    public function create()
    {
        return view('dashboard.owners.create');
    }

    public function store(Request $request)
    {
        // Validation rules
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:8',
            'address' => 'required',
            'image' => 'max:2048', // Adjust this based on your requirements
        ]);

        // Create user
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'image' => $request->image ?? null,

        ])->assignRole('owner');

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $path = $image->store('public/images/users');
            $imageUrl = Storage::url($path);
            $user->image = $imageUrl;
            $user->save();
        }

        // Create owner
        $user->owner()->create([
            'address' => $request->address,
        ]);

        return redirect()->route('owners.index')->with('success', 'Owner created successfully');
    }


    public function show(Owner $owner)
    {
        return view('dashboard.owners.show', compact('owner'));
    }

    public function edit(Owner $owner)
    {
        return view('dashboard.owners.edit', compact('owner'));
    }

    public function update(Request $request, Owner $owner)
    {
        $request->validate([
            'address' => 'required',
            'user_id' => 'nullable|exists:users,id',
            'image' => 'max:2048', // Adjust this based on your requirements
        ]);

        // Update owner details
        $owner->update([
            'address' => $request->address,
        ]);

        // Update user details
        $user = $owner->user;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = $request->password ? Hash::make($request->password) : $user->password;

        // Handle image update
        if ($request->hasFile('image')) {
            // Delete the old image if it exists
            if ($user->image) {
                $imageName = basename($user->image);
                Storage::delete('public/images/users/' . $imageName);
            }


            // Upload the new image
            $image = $request->file('image');
            $path = $image->store('public/images/users');
            $imageUrl = Storage::url($path);
            $user->image = $imageUrl;
        }

        $user->save();

        return redirect()->route('owners.index')->with('success', 'Owner updated successfully');
    }

    public function destroy(Owner $owner)
    {
        $owner->delete();

        return redirect()->route('owners.index')
            ->with('success', 'Owner deleted successfully');
    }

    public function profile_update(Request $request, Owner $owner)
    {
        $request->validate([
            'address' => 'required',
            'user_id' => 'nullable|exists:users,id',
            'image' => 'max:2048', // Adjust this based on your requirements
        ]);

        // Update owner details
        $owner->update([
            'address' => $request->address,
        ]);

        // Update user details
        $user = $owner->user;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = $request->password ? Hash::make($request->password) : $user->password;

        // Handle image update
        if ($request->hasFile('image')) {
            // Delete the old image if it exists
            if ($user->image) {
                $imageName = basename($user->image);
                Storage::delete('public/images/users/' . $imageName);
            }


            // Upload the new image
            $image = $request->file('image');
            $path = $image->store('public/images/users');
            $imageUrl = Storage::url($path);
            $user->image = $imageUrl;
        }

        $user->save();

        return redirect()->route('owners.index')->with('success', 'Owner updated successfully');
    }
}
