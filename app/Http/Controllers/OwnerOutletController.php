<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Outlet;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OwnerOutletController extends Controller
{
    public function form($user_id)
    {
        $user = User::find($user_id);
        $outlets = Outlet::all();

        return view('dashboard.owner_outlets.form', compact('user', 'outlets'));
    }

    public function submit(Request $request, $user_id)
    {
        $user = User::find($user_id);

        $user->outlets()->sync($request->outlets); // Menghubungkan user dengan outlet yang dipilih

        return back()->with('success', 'Outlets updated successfully for user');

    }
}
