<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BookController extends Controller
{
    public function index()
    {
        $books = Book::all();
        return view('dashboard.books.index', compact('books'));
    }

    public function create()
    {
        return view('dashboard.books.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
            'author' => 'nullable',
            'publisher' => 'nullable',
            'available' => 'boolean',
            'quantity' => 'integer',
            'description' => 'nullable',
        ]);

        $data = $request->all();

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $path = $image->store('public/images/books');
            $imageUrl = Storage::url($path);
            $data['image'] = $imageUrl;
        }

        Book::create($data);

        return redirect()->route('books.index')
            ->with('success', 'Book created successfully.');
    }

    public function show(Book $book)
    {
        return view('dashboard.books.show', compact('book'));
    }

    public function edit(Book $book)
    {
        return view('dashboard.books.edit', compact('book'));
    }

    public function update(Request $request, Book $book)
    {
        $request->validate([
            'title' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
            'author' => 'nullable',
            'publisher' => 'nullable',
            'available' => 'boolean',
            'quantity' => 'integer',
            'description' => 'nullable',
        ]);

        $data = $request->all();

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $path = $image->store('public/images/books');
            $imageUrl = Storage::url($path);
            $data['image'] = $imageUrl;

            if ($book->image) {
                $imageName = basename($book->image);
                Storage::delete('public/images/books/' . $imageName);
            }
        }

        $book->update($data);

        return redirect()->route('books.index')
            ->with('success', 'Book updated successfully.');
    }

    public function destroy(Book $book)
    {
        if ($book->image) {
            $imageName = basename($book->image);
            Storage::delete('public/images/books/' . $imageName);
        }

        $book->delete();

        return redirect()->route('books.index')
            ->with('success', 'Book deleted successfully.');
    }
}
