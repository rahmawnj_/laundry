<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Outlet extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'city',
        'address',
        'code',
        'username',
        'password',
        'logo',
        'api_token'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
