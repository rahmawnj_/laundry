<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = ['outlet_id', 'amount', 'time', 'rfid', 'status'];

protected $dates = ['time'];

    public function outlet()
    {
        return $this->belongsTo(Outlet::class);
    }
}


