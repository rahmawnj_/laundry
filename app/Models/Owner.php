<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Owner extends Model
{
    use HasFactory, SoftDeletes;


    protected $fillable = ['address', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function transactions()
    {
        return $this->hasManyThrough(
            Transaction::class,
            Outlet::class,
            'id', // Foreign key pada tabel outlets
            'outlet_id', // Foreign key pada tabel transactions
            'user_id', // Lokal key pada tabel owners
            'id' // Lokal key pada tabel outlets
        );
    }

    
}
