<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('outlet_id')->nullable();
            $table->decimal('amount');
            $table->timestamp('time')->default(now()); // Menggunakan nilai default 'now()'
            $table->string('rfid');
            $table->enum('status', ['success', 'error'])->default('success');
            $table->timestamps();

            $table->foreign('outlet_id')
                ->references('id')
                ->on('outlets')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transactions');
    }
};
