<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('owners', function (Blueprint $table) {
            $table->id();
            $table->string('address');
            $table->unsignedBigInteger('user_id')->nullable(); // Tambahkan kolom user_id dan set nullable

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade'); // Set foreign key dengan onDelete('set null')
            $table->timestamps();

            $table->softDeletes(); // Ini akan menambahkan kolom 'deleted_at'

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('owners');
    }
};
