<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('outlets', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('city');
            $table->string('address')->nullable();
            $table->string('code');
            $table->string('username')->unique();
            $table->string('password');
            $table->string('logo')->nullable();
            $table->timestamps();

            $table->string('api_token', 60)->unique()->nullable();


            // Menambahkan indeks gabungan pada 'code' dan 'city'
            $table->unique(['code', 'city']);

            $table->softDeletes(); // Ini akan menambahkan kolom 'deleted_at'

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('outlets');
    }
};
