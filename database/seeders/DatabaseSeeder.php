<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\JobCategory;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Support\Facades\Hash;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {

        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        $admin = Role::create(['name' => 'admin']);
        $owner = Role::create(['name' => 'owner']);

        User::create([
            'name' => 'admin',
            'email' => 'admin@mail.com',
            'password' => Hash::make('admin@mail.com'),
        ])->assignRole('admin');

        Permission::create(['name' => 'users.index']);
        Permission::create(['name' => 'users.show']);
        Permission::create(['name' => 'users.edit']);
        Permission::create(['name' => 'users.create']);
        Permission::create(['name' => 'users.update']);
        Permission::create(['name' => 'users.store']);
        Permission::create(['name' => 'users.destroy']);

        Permission::create(['name' => 'owners.index']);
        Permission::create(['name' => 'owners.show']);
        Permission::create(['name' => 'owners.edit']);
        Permission::create(['name' => 'owners.create']);
        Permission::create(['name' => 'owners.update']);
        Permission::create(['name' => 'owners.store']);
        Permission::create(['name' => 'owners.destroy']);

        Permission::create(['name' => 'outlets.index']);
        Permission::create(['name' => 'outlets.show']);
        Permission::create(['name' => 'outlets.edit']);
        Permission::create(['name' => 'outlets.create']);
        Permission::create(['name' => 'outlets.update']);
        Permission::create(['name' => 'outlets.store']);
        Permission::create(['name' => 'outlets.destroy']);

        Permission::create(['name' => 'owner_outlets.form']);
        Permission::create(['name' => 'owner_outlets.submit']);
        Permission::create(['name' => 'profile.form']);
        Permission::create(['name' => 'profile.submit']);

        Permission::create(['name' => 'histories']);

        $owner->givePermissionTo(['outlets.index', 'outlets.show', 'outlets.edit', 'outlets.update', 'histories', 'profile.form', 'profile.submit']);
        $admin->syncPermissions(Permission::all());
    }
}
