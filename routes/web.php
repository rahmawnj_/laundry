<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\OwnerController;
use App\Http\Controllers\OutletController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\OwnerOutletController;
use App\Http\Controllers\TransactionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Auth::routes();

Route::get('/', function () {
    return redirect()->route('login');
});
Route::get('/home', function () {
    return redirect()->route('histories');
})->name('home');

// routes/.php


Route::group(['middleware' => ['auth', 'permission']], function () {
    // Route::get('/dashboard', function () {
    //     return view('dashboard.index');
    // })->name('dashboard');
    
    Route::resource('users', UserController::class);
    Route::resource('outlets', OutletController::class);
    Route::resource('owners', OwnerController::class);
    
    Route::resource('roles', RoleController::class);
    Route::resource('permissions', PermissionController::class);
    
    Route::get('owner_outlets/{user_id}/form', [OwnerOutletController::class, 'form'])->name('owner_outlets.form');
    Route::put('owner_outlets/{user_id}', [OwnerOutletController::class, 'submit'])->name('owner_outlets.submit');

    Route::get('profile', [UserController::class, 'form'])->name('profile.form');
    Route::put('profile', [UserController::class, 'submit'])->name('profile.submit');

    Route::get('/transactions', [TransactionController::class, 'index'])->name('histories');
});
