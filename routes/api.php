<?php

use App\Http\Controllers\OutletController;
use App\Http\Controllers\TransactionController;
use Carbon\Carbon;
use App\Models\Student;
use App\Models\Employee;
use Carbon\CarbonInterval;
use Illuminate\Http\Request;
use App\Models\CompanyGallery;
use App\Models\EmployeeAttendance;
use App\Models\StudentAttendance;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('outlet/login', [OutletController::class, 'login']);
Route::get('check-token/{token}', [OutletController::class, 'checkToken']);
Route::post('add-transaction', [TransactionController::class, 'addTransaction']);
