<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title> -- | Forbidden</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- ================== BEGIN core-css ================== -->
    <link href="{{ asset('assets/css/vendor.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/facebook/app.min.css') }}" rel="stylesheet" />
    @stack('styles')
    <!-- ================== END core-css ================== -->
</head>

<body>
    <!-- BEGIN #loader -->
    {{-- <div id="loader" class="app-loader">
        <span class="spinner"></span>
    </div> --}}
    <!-- END #loader -->

    <!-- BEGIN #app -->
    <div id="app" class="app app-header-fixed app-sidebar-fixed">
        <!-- BEGIN #header -->
        <div class="error">
            <div class="error-code">403</div>
            <div class="error-content">
                <div class="error-message">You are not allowed...</div>
              
                <div>
                    <a href="index.html" class="btn btn-success px-3">Go Home</a>
                </div>
            </div>
        </div>
    </div>
    <!-- END #app -->

    <!-- ================== BEGIN core-js ================== -->
    <script src="{{ asset('assets/js/vendor.min.js') }}"></script>
    <script src="{{ asset('assets/js/app.min.js') }}"></script>
    <script src="{{ asset('assets/js/theme/facebook.min.js') }}"></script>
    <!-- ================== END core-js ================== -->
</body>

</html>
