<div class="col-xl-3 col-lg-4 col-md-12 rightSidebar m-b30">

    <div class="side-bar-st-1">

        <div class="twm-candidate-profile-pic">

            <img src="{{ !Auth::user()->image ? asset('assets/images/default-profile.jpg') : asset('storage/' . Auth::user()->image) }}"
                alt="">


        </div>
        <div class="twm-mid-content text-center">
            <a href="candidate-detail.html" class="twm-job-title">
                <h4>{{ Auth::user()->name }} </h4>
            </a>
            <p>{{ Auth::user()->email }}</p>
        </div>

        <div class="twm-nav-list-1">
            <ul>
                <li class="{{ $title == 'Dashboard' ? 'active' : '' }}"><a href="{{ route('h.candidate-dashboard') }}"><i
                            class="fas fa-tachometer-alt"></i> Dashboard</a>
                </li>
                <li class="{{ $title == 'Account' ? 'active' : '' }}"><a href="{{ route('h.candidate-account') }}"><i
                            class="fas fa-user-cog"></i> Account</a></li>
                <li class="{{ $title == 'Profile' ? 'active' : '' }}"><a href="{{ route('h.candidate-profile') }}"><i
                            class="fas fa-id-card"></i> Profile</a></li>
                <li class="{{ $title == 'Resume' ? 'active' : '' }}"><a href="{{ route('h.candidate-resume') }}"><i
                            class="far fa-file-alt"></i> Resume</a></li>
                <li class="{{ $title == 'Job Applications' ? 'active' : '' }}"><a
                        href="{{ route('h.candidate-jobapplications.index') }}"><i class="fas fa-briefcase"></i> Job
                        Applications</a></li>
                <li class="{{ $title == 'Job Offers' ? 'active' : '' }}"><a
                        href="{{ route('candidate-joboffers.index') }}"><i class="fas fa-handshake"></i> Job
                        Offers</a></li>
            </ul>
        </div>

    </div>

</div>
