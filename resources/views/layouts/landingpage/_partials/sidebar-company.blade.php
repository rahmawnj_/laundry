<div class="col-xl-3 col-lg-4 col-md-12 rightSidebar m-b30">

    <div class="side-bar-st-1">

        <div class="twm-candidate-profile-pic">

            <img src="{{ !Auth::user()->image ? asset('assets/images/default-profile.jpg') : asset('storage/' . Auth::user()->image) }}"
                alt="">


        </div>

        <div class="twm-mid-content text-center">
            <a href="candidate-detail.html" class="twm-job-title">
                <h4>{{ Auth::user()->name }}</h4>
            </a>
            <p>{{ Auth::user()->email }}</p>
        </div>

        <div class="twm-nav-list-1">
            <ul>
                <li class="{{ $title == 'Dashboard' ? 'active' : '' }}"><a href="{{ route('h.company-dashboard') }}"><i
                            class="fas fa-tachometer-alt"></i> Dashboard</a></li>
                <li class="{{ $title == 'Account' ? 'active' : '' }}"><a href="{{ route('h.company-account') }}"><i
                            class="fas fa-user-cog"></i> Account</a></li>
                <li class="{{ $title == 'Profile' ? 'active' : '' }}"><a href="{{ route('h.company-profile') }}"><i
                            class="fas fa-id-card"></i> Profile</a></li>
                <li class="{{ $title == 'Jobs' ? 'active' : '' }}"><a href="{{ route('h.company-jobs.index') }}"><i
                            class="fas fa-briefcase"></i> Jobs</a></li>
                <li class="{{ $title == 'Applications' ? 'active' : '' }}"><a
                        href="{{ route('h.company-applications.index') }}"><i class="fas fa-file-alt"></i>
                        Applications</a></li>
                <li class="{{ $title == 'Job Offers' ? 'active' : '' }}"><a
                        href="{{ route('company-joboffers.index') }}"><i class="fas fa-handshake"></i> Job
                        Offers</a></li>
            </ul>
        </div>

    </div>

</div>
