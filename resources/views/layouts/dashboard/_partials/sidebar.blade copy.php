<div id="sidebar" class="app-sidebar" style="background-color: white">
    <!-- BEGIN scrollbar -->
    <div class="app-sidebar-content" data-scrollbar="true" data-height="100%">
        <!-- BEGIN menu -->
        <div class="menu">
            <div class="menu-profile">
                <a href="javascript:;" class="menu-profile-link" data-toggle="app-sidebar-profile"
                    data-target="#appSidebarProfileMenu">
                    <div class="menu-profile-cover with-shadow"></div>
                    <div class="menu-profile-image">
                        <img src="{{ asset(Auth::user()->image ?? 'assets/img/default-profile.jpg') }}" alt="" />
                    </div>
                    <div class="menu-profile-info">
                        <div class="d-flex align-items-center">
                            <div class="flex-grow-1">
                                {{ Auth::user()->name }}
                            </div>
                            <div class="menu-caret ms-auto"></div>
                        </div>
                        <small>{{ Auth::user()->email }}</small>
                    </div>
                </a>
            </div>
            <div id="appSidebarProfileMenu" class="collapse">
                <div class="menu-item pt-5px">
                    <a href="javascript:;" class="menu-link">
                        <div class="menu-icon"><i class="fa fa-cog"></i></div>
                        <div class="menu-text">Profile</div>
                    </a>
                </div>
                <div class="menu-item pt-5px">
                    <a href="javascript:;" class="menu-link">
                        <div class="menu-icon"><i class="fa fa-cog"></i></div>
                        <div class="menu-text">Settings</div>
                    </a>
                </div>
                <div class="menu-item">
                    <a href="javascript:;" class="menu-link">
                        <div class="menu-icon"><i class="fa fa-pencil-alt"></i></div>
                        <div class="menu-text"> Send Feedback</div>
                    </a>
                </div>
                <div class="menu-item pb-5px">
                    <a href="javascript:;" class="menu-link">
                        <div class="menu-icon"><i class="fa fa-question-circle"></i></div>
                        <div class="menu-text"> Helps</div>
                    </a>
                </div>
                <div class="menu-divider m-0"></div>
            </div>
            <div class="menu-header">Navigation</div>
            <div class="menu-item has-sub">
                <a href="javascript:;" class="menu-link">
                    <div class="menu-icon">
                        <i class="fa fa-th-large"></i>
                    </div>
                    <div class="menu-text">Dashboard</div>
                    <div class="menu-caret"></div>
                </a>
                <div class="menu-submenu">
                    <div class="menu-item">
                        <a href="index.html" class="menu-link">
                            <div class="menu-text">E-Money</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="index_v2.html" class="menu-link">
                            <div class="menu-text">E-Library</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="index_v3.html" class="menu-link">
                            <div class="menu-text">E-Attendance</div>
                        </a>
                    </div>
                </div>
            </div>

            {{-- <div class="menu-item has-sub">
                <a href="javascript:;" class="menu-link">
                    <div class="menu-icon">
                        <i class="fa fa-hdd"></i>
                    </div>
                    <div class="menu-text">Email</div>
                    <div class="menu-badge">10</div>
                </a>
                <div class="menu-submenu">
                    <div class="menu-item">
                        <a href="email_inbox.html" class="menu-link">
                            <div class="menu-text">Inbox</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="email_compose.html" class="menu-link">
                            <div class="menu-text">Compose</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="email_detail.html" class="menu-link">
                            <div class="menu-text">Detail</div>
                        </a>
                    </div>
                </div>
            </div> --}}



            <div class="menu-item ">
                <a href="bootstrap_5.html" class="menu-link">
                    <div class="menu-icon-img">
                        <img src="../assets/img/logo/logo-bs5.png" alt="" />
                    </div>
                    <div class="menu-text">Notification</div>
                </a>
            </div>

            <div class="menu-header">Data Management</div>

            <div class="menu-item has-sub">
                <a href="javascript:;" class="menu-link">
                    <div class="menu-icon">
                        <i class="fa fa-list-ol"></i>
                    </div>
                    <div class="menu-text">Data Master</div>
                    <div class="menu-caret"></div>
                </a>
                <div class="menu-submenu">
                    <div class="menu-item">
                        <a href="form_elements.html" class="menu-link">
                            <div class="menu-text">Users <i class="fa fa-paper-plane text-theme"></i>
                            </div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="form_plugins.html" class="menu-link">
                            <div class="menu-text">Students <i class="fa fa-paper-plane text-theme"></i>
                            </div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="form_slider_switcher.html" class="menu-link">
                            <div class="menu-text">Employees</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="form_validation.html" class="menu-link">
                            <div class="menu-text">Classrooms</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="form_wizards.html" class="menu-link">
                            <div class="menu-text">Positions <i class="fa fa-paper-plane text-theme"></i></div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="form_wysiwyg.html" class="menu-link">
                            <div class="menu-text">Merchants</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="form_editable.html" class="menu-link">
                            <div class="menu-text">Products</div>
                        </a>
                    </div>

                </div>
            </div>

            <div class="menu-item has-sub">
                <a href="javascript:;" class="menu-link">
                    <div class="menu-icon">
                        <i class="fa fa-cash-register"></i>
                    </div>
                    <div class="menu-text">Authentications <span class="menu-label">NEW</span></div>
                    <div class="menu-caret"></div>
                </a>
                <div class="menu-submenu">
                    <div class="menu-item">
                        <a href="pos_customer_order.html" target="_blank" class="menu-link">
                            <div class="menu-text">Roles</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="pos_kitchen_order.html" target="_blank" class="menu-link">
                            <div class="menu-text">Permissions</div>
                        </a>
                    </div>

                </div>
            </div>

            <div class="menu-header">Transactions</div>
            <div class="menu-header">Reports</div>
            <div class="menu-header">Setting</div>

            <div class="menu-item ">
                <a href="bootstrap_5.html" class="menu-link">
                    <div class="menu-icon-img">
                        <img src="../assets/img/logo/logo-bs5.png" alt="" />
                    </div>
                    <div class="menu-text">Log Activity</div>
                </a>
            </div>

            <div class="menu-item has-sub">
                <a href="javascript:;" class="menu-link">
                    <div class="menu-icon">
                        <i class="fa fa-star"></i>
                    </div>
                    <div class="menu-text">Front End <span class="menu-label">NEW</span></div>
                    <div class="menu-caret"></div>
                </a>
                <div class="menu-submenu">
                    <div class="menu-item">
                        <a href="../../../frontend/template/template_one_page_parallax/index.html" target="_blank"
                            class="menu-link">
                            <div class="menu-text">One Page Parallax</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="../../../frontend/template/template_blog/index.html" target="_blank"
                            class="menu-link">
                            <div class="menu-text">Blog</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="../../../frontend/template/template_forum/index.html" target="_blank"
                            class="menu-link">
                            <div class="menu-text">Forum</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="../../../frontend/template/template_e_commerce/index.html" target="_blank"
                            class="menu-link">
                            <div class="menu-text">E-Commerce</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="../../../frontend/template/template_corporate/index.html" target="_blank"
                            class="menu-link">
                            <div class="menu-text">Corporate <i class="fa fa-paper-plane text-theme"></i>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="menu-item has-sub">
                <a href="javascript:;" class="menu-link">
                    <div class="menu-icon">
                        <i class="fa fa-envelope"></i>
                    </div>
                    <div class="menu-text">Email Template</div>
                    <div class="menu-caret"></div>
                </a>
                <div class="menu-submenu">
                    <div class="menu-item">
                        <a href="email_system.html" class="menu-link">
                            <div class="menu-text">System Template</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="email_newsletter.html" class="menu-link">
                            <div class="menu-text">Newsletter Template</div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="menu-item has-sub">
                <a href="javascript:;" class="menu-link">
                    <div class="menu-icon">
                        <i class="fa fa-chart-pie"></i>
                    </div>
                    <div class="menu-text">Chart</div>
                    <div class="menu-caret"></div>
                </a>
                <div class="menu-submenu">
                    <div class="menu-item">
                        <a href="chart-flot.html" class="menu-link">
                            <div class="menu-text">Flot Chart</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="chart-js.html" class="menu-link">
                            <div class="menu-text">Chart JS</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="chart-d3.html" class="menu-link">
                            <div class="menu-text">d3 Chart</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="chart-apex.html" class="menu-link">
                            <div class="menu-text">Apex Chart</div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="menu-item">
                <a href="calendar.html" class="menu-link">
                    <div class="menu-icon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <div class="menu-text">Calendar</div>
                </a>
            </div>
            <div class="menu-item has-sub">
                <a href="javascript:;" class="menu-link">
                    <div class="menu-icon">
                        <i class="fa fa-map"></i>
                    </div>
                    <div class="menu-text">Map</div>
                    <div class="menu-caret"></div>
                </a>
                <div class="menu-submenu">
                    <div class="menu-item">
                        <a href="map_vector.html" class="menu-link">
                            <div class="menu-text">Vector Map</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="map_google.html" class="menu-link">
                            <div class="menu-text">Google Map</div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="menu-item has-sub">
                <a href="javascript:;" class="menu-link">
                    <div class="menu-icon">
                        <i class="fa fa-image"></i>
                    </div>
                    <div class="menu-text">Gallery</div>
                    <div class="menu-caret"></div>
                </a>
                <div class="menu-submenu">
                    <div class="menu-item">
                        <a href="gallery.html" class="menu-link">
                            <div class="menu-text">Gallery v1</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="gallery_v2.html" class="menu-link">
                            <div class="menu-text">Gallery v2</div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="menu-item has-sub active">
                <a href="javascript:;" class="menu-link">
                    <div class="menu-icon">
                        <i class="fa fa-cogs"></i>
                    </div>
                    <div class="menu-text">Page Options <span class="menu-label">NEW</span></div>
                    <div class="menu-caret"></div>
                </a>
                <div class="menu-submenu">
                    <div class="menu-item active">
                        <a href="page_blank.html" class="menu-link">
                            <div class="menu-text">Blank Page</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="page_with_footer.html" class="menu-link">
                            <div class="menu-text">Page with Footer</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="page_with_fixed_footer.html" class="menu-link">
                            <div class="menu-text">Page with Fixed Footer <i class="fa fa-paper-plane text-theme"></i>
                            </div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="page_without_sidebar.html" class="menu-link">
                            <div class="menu-text">Page without Sidebar</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="page_with_right_sidebar.html" class="menu-link">
                            <div class="menu-text">Page with Right Sidebar</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="page_with_minified_sidebar.html" class="menu-link">
                            <div class="menu-text">Page with Minified Sidebar</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="page_with_two_sidebar.html" class="menu-link">
                            <div class="menu-text">Page with Two Sidebar</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="page_with_line_icons.html" class="menu-link">
                            <div class="menu-text">Page with Line Icons</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="page_with_ionicons.html" class="menu-link">
                            <div class="menu-text">Page with Ionicons</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="page_full_height.html" class="menu-link">
                            <div class="menu-text">Full Height Content</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="page_with_wide_sidebar.html" class="menu-link">
                            <div class="menu-text">Page with Wide Sidebar</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="page_with_mega_menu.html" class="menu-link">
                            <div class="menu-text">Page with Mega Menu</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="page_with_top_menu.html" class="menu-link">
                            <div class="menu-text">Page with Top Menu</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="page_with_boxed_layout.html" class="menu-link">
                            <div class="menu-text">Page with Boxed Layout</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="page_with_mixed_menu.html" class="menu-link">
                            <div class="menu-text">Page with Mixed Menu</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="page_boxed_layout_with_mixed_menu.html" class="menu-link">
                            <div class="menu-text">Boxed Layout with Mixed Menu</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="page_with_search_sidebar.html" class="menu-link">
                            <div class="menu-text">Page with Search Sidebar <i
                                    class="fa fa-paper-plane text-theme"></i></div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="menu-item has-sub">
                <a href="javascript:;" class="menu-link">
                    <div class="menu-icon">
                        <i class="fa fa-gift"></i>
                    </div>
                    <div class="menu-text">Extra <span class="menu-label">NEW</span></div>
                    <div class="menu-caret"></div>
                </a>
                <div class="menu-submenu">
                    <div class="menu-item">
                        <a href="extra_timeline.html" class="menu-link">
                            <div class="menu-text">Timeline</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="extra_coming_soon.html" class="menu-link">
                            <div class="menu-text">Coming Soon Page</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="extra_search_results.html" class="menu-link">
                            <div class="menu-text">Search Results</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="extra_invoice.html" class="menu-link">
                            <div class="menu-text">Invoice</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="extra_404_error.html" class="menu-link">
                            <div class="menu-text">404 Error Page</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="extra_profile.html" class="menu-link">
                            <div class="menu-text">Profile Page</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="extra_scrum_board.html" class="menu-link">
                            <div class="menu-text">Scrum Board <i class="fa fa-paper-plane text-theme"></i>
                            </div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="extra_cookie_acceptance_banner.html" class="menu-link">
                            <div class="menu-text">Cookie Acceptance Banner <i
                                    class="fa fa-paper-plane text-theme"></i></div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="menu-item has-sub">
                <a href="javascript:;" class="menu-link">
                    <div class="menu-icon">
                        <i class="fa fa-key"></i>
                    </div>
                    <div class="menu-text">Login & Register</div>
                    <div class="menu-caret"></div>
                </a>
                <div class="menu-submenu">
                    <div class="menu-item">
                        <a href="login.html" class="menu-link">
                            <div class="menu-text">Login</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="login_v2.html" class="menu-link">
                            <div class="menu-text">Login v2</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="login_v3.html" class="menu-link">
                            <div class="menu-text">Login v3</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="register_v3.html" class="menu-link">
                            <div class="menu-text">Register v3</div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="menu-item has-sub">
                <a href="javascript:;" class="menu-link">
                    <div class="menu-icon">
                        <i class="fa fa-cubes"></i>
                    </div>
                    <div class="menu-text">Version <span class="menu-label">NEW</span></div>
                    <div class="menu-caret"></div>
                </a>
                <div class="menu-submenu">
                    <div class="menu-item">
                        <a href="../template_html/index_v3.html" class="menu-link">
                            <div class="menu-text">HTML</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="../template_ajax/" class="menu-link">
                            <div class="menu-text">AJAX</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="../template_angularjs/" class="menu-link">
                            <div class="menu-text">ANGULAR JS</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="../template_angularjs10/" class="menu-link">
                            <div class="menu-text">ANGULAR JS 10</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="javascript:alert('Laravel Version only available in downloaded version.');"
                            class="menu-link">
                            <div class="menu-text">LARAVEL</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="../template_vuejs/" class="menu-link">
                            <div class="menu-text">VUE JS</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="../template_reactjs/" class="menu-link">
                            <div class="menu-text">REACT JS</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="javascript:alert('.NET Core 3.1 MVC Version only available in downloaded version.');"
                            class="menu-link">
                            <div class="menu-text">ASP.NET <i class="fa fa-paper-plane text-theme"></i></div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="../template_material/index_v3.html" class="menu-link">
                            <div class="menu-text">MATERIAL DESIGN</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="../template_apple/index_v3.html" class="menu-link">
                            <div class="menu-text">APPLE DESIGN</div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="../template_transparent/index_v3.html" class="menu-link">
                            <div class="menu-text">TRANSPARENT DESIGN <i class="fa fa-paper-plane text-theme"></i>
                            </div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="../template_facebook/index_v3.html" class="menu-link">
                            <div class="menu-text">FACEBOOK DESIGN <i class="fa fa-paper-plane text-theme"></i></div>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a href="../template_google/index_v3.html" class="menu-link">
                            <div class="menu-text">GOOGLE DESIGN <i class="fa fa-paper-plane text-theme"></i>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="menu-item has-sub">
                <a href="javascript:;" class="menu-link">
                    <div class="menu-icon">
                        <i class="fa fa-medkit"></i>
                    </div>
                    <div class="menu-text">Helper</div>
                    <div class="menu-caret"></div>
                </a>
                <div class="menu-submenu">
                    <div class="menu-item">
                        <a href="helper_css.html" class="menu-link">
                            <div class="menu-text">Predefined CSS Classes</div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="menu-item has-sub">
                <a href="javascript:;" class="menu-link">
                    <div class="menu-icon">
                        <i class="fa fa-align-left"></i>
                    </div>
                    <div class="menu-text">Menu Level</div>
                    <div class="menu-caret"></div>
                </a>
                <div class="menu-submenu">
                    <div class="menu-item has-sub">
                        <a href="javascript:;" class="menu-link">
                            <div class="menu-text">Menu 1.1</div>
                            <div class="menu-caret"></div>
                        </a>
                        <div class="menu-submenu">
                            <div class="menu-item has-sub">
                                <a href="javascript:;" class="menu-link">
                                    <div class="menu-text">Menu 2.1</div>
                                    <div class="menu-caret"></div>
                                </a>
                                <div class="menu-submenu">
                                    <div class="menu-item"><a href="javascript:;" class="menu-link">
                                            <div class="menu-text">Menu 3.1</div>
                                        </a></div>
                                    <div class="menu-item"><a href="javascript:;" class="menu-link">
                                            <div class="menu-text">Menu 3.2</div>
                                        </a></div>
                                </div>
                            </div>
                            <div class="menu-item"><a href="javascript:;" class="menu-link">
                                    <div class="menu-text">Menu 2.2</div>
                                </a></div>
                            <div class="menu-item"><a href="javascript:;" class="menu-link">
                                    <div class="menu-text">Menu 2.3</div>
                                </a></div>
                        </div>
                    </div>
                    <div class="menu-item"><a href="javascript:;" class="menu-link">
                            <div class="menu-text">Menu 1.2</div>
                        </a></div>
                    <div class="menu-item"><a href="javascript:;" class="menu-link">
                            <div class="menu-text">Menu 1.3</div>
                        </a></div>
                </div>
            </div>

            <!-- BEGIN minify-button -->
            <div class="menu-item d-flex">
                <a href="javascript:;" class="app-sidebar-minify-btn ms-auto" data-toggle="app-sidebar-minify"><i
                        class="fa fa-angle-double-left"></i></a>
            </div>
            <!-- END minify-button -->
        </div>
        <!-- END menu -->
    </div>
    <!-- END scrollbar -->
</div>
