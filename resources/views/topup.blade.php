@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif

@if (session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
@endif

<form action="{{ route('topup.process') }}" method="POST">
    @csrf
    <div class="form-group">
        <label for="amount">Jumlah Top-up</label>
        <input type="number" name="amount" id="amount" class="form-control" placeholder="Jumlah Top-up" required>
    </div>
    <button type="submit" class="btn btn-primary">Top-up</button>
</form>
