@extends('layouts.landingpage.app')

@section('title', 'Register')

@section('container')
    <div class="page-content">

        <!-- register Section Start -->
        <div class="section-full site-bg-white">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-8 col-lg-6 col-md-5 twm-log-reg-media-wrap">
                        <div class="twm-log-reg-media">
                            <div class="twm-l-media">
                                <img src="{{ asset('assets/images/login-bg.png') }}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-7">
                        <div class="twm-log-reg-form-wrap">
                            <div class="twm-log-reg-logo-head">
                                <a href="{{ route('home') }}">
                                    <img src="{{ asset('storage/' . App\Models\Setting::where('key', 'logotype')->first()->value) }}"
                                        alt="" class="logo">
                                </a>
                            </div>
                            <div class="twm-log-reg-inner">
                                <div class="twm-log-reg-head">
                                    <div class="twm-log-reg-logo">
                                        <span class="log-reg-form-title">Register</span>
                                    </div>
                                </div>
                                <div class="twm-tabs-style-2">
                                    <ul class="nav nav-tabs" id="myTab2" role="tablist">
                                        <!--register Candidate-->
                                        <li class="nav-item">
                                            <button class="nav-link active" data-bs-toggle="tab"
                                                data-bs-target="#twm-register-candidate" type="button"><i
                                                    class="fas fa-user-tie"></i>Candidate</button>
                                        </li>
                                        <!--register Employer-->
                                        <li class="nav-item">
                                            <button class="nav-link" data-bs-toggle="tab"
                                                data-bs-target="#twm-register-Employer" type="button"><i
                                                    class="fas fa-building"></i>Employer</button>
                                        </li>
                                    </ul>
                                    <div class="tab-content" id="myTab2Content">
                                        <!--register Candidate Content-->
                                        <div class="tab-pane fade show active" id="twm-register-candidate">
                                            <div class="row">

                                                <form action="{{ route('register') }}" method="post">
                                                    @csrf
                                                    <input type="hidden" name="role" value="candidate">
                                                    <div class="col-lg-12">
                                                        <div class="form-group mb-3">
                                                            <input name="name" type="text" value="{{ old('name') }}"
                                                                class="form-control" placeholder="Full Name*">
                                                            @error('name')
                                                                <span class="text-danger" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="form-group mb-3">
                                                            <input name="email" type="text" value="{{ old('email') }}"
                                                                class="form-control" placeholder="Email*">
                                                            @error('email')
                                                                <span class="text-danger" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="form-group mb-3">
                                                            <input name="password" type="password" class="form-control"
                                                                placeholder="Password*">
                                                            @error('password')
                                                                <span class="text-danger" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="form-group mb-3">
                                                            <input name="password_confirmation" type="password"
                                                                class="form-control" placeholder="Password Confirmation*">
                                                            @error('password_confirmation')
                                                                <span class="text-danger" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <button type="submit" class="site-button">Register</button>
                                                        </div>
                                                    </div>

                                                </form>


                                            </div>
                                        </div>
                                        <!--register Employer Content-->
                                        <div class="tab-pane fade" id="twm-register-Employer">
                                            <div class="row">

                                                <form action="{{ route('register') }}" method="post">
                                                    @csrf
                                                    <input type="hidden" name="role" value="employer">
                                                    <div class="col-lg-12">
                                                        <div class="form-group mb-3">
                                                            <input name="name" type="text"
                                                                value="{{ old('name') }}" class="form-control"
                                                                placeholder="Company Name*">
                                                            @error('name')
                                                                <span class="text-danger" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="form-group mb-3">
                                                            <input name="email" type="text"
                                                                value="{{ old('email') }}" class="form-control"
                                                                placeholder="Email*">
                                                            @error('email')
                                                                <span class="text-danger" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="form-group mb-3">
                                                            <input name="password" type="password" class="form-control"
                                                                placeholder="Password*">
                                                            @error('password')
                                                                <span class="text-danger" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="form-group mb-3">
                                                            <input name="password_confirmation" type="password"
                                                                class="form-control" placeholder="Password Confirmation*">
                                                            @error('password_confirmation')
                                                                <span class="text-danger" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <button type="submit" class="site-button">Register</button>
                                                        </div>
                                                    </div>

                                                </form>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- register Section End -->
    </div>
@endsection
