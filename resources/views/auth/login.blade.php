<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Top Up</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- ================== BEGIN core-css ================== -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <link href="{{ asset('assets/css/vendor.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/facebook/app.min.css') }}" rel="stylesheet" />
    <!-- ================== END core-css ================== -->
</head>

<body class='pace-top'>
    <!-- BEGIN #loader -->

    <!-- END #loader -->

    <!-- BEGIN #app -->
    <div id="app" class="app">
        <!-- BEGIN login -->
        <div class="login login-with-news-feed">
            <!-- BEGIN news-feed -->
            <div class="news-feed">
                <div class="news-image" style="background-image: url(../assets/img/login-bg/login-bg-11.jpg)"></div>
                <div class="news-caption">
                    <h4 class="caption-title"><b>Laundry</b></h4>

                </div>
            </div>
            <!-- END news-feed -->

            <!-- BEGIN login-container -->
            <div class="login-container">
                <!-- BEGIN login-header -->
                <div class="login-header mb-30px">
                    <div class="brand">
                        <div class="d-flex align-items-center">
                            <i class="fas fa-store-alt fa-lg me-3"></i>
                            <b>Laundry</b>
                        </div>

                    </div>
                    <div class="icon">
                        <i class="fa fa-sign-in-alt"></i>
                    </div>
                </div>
                <!-- END login-header -->

                <!-- BEGIN login-content -->
                <div class="login-content">
                    <form action="{{ route('login') }}" method="POST" class="fs-13px">
                        @csrf
                        <div class="form-floating mb-15px">
                            <input name="email" type="text" value="{{ old('email') }}"
                                class="form-control h-45px fs-13px" placeholder="Email Address" id="emailAddress" />
                            <label for="emailAddress" class="d-flex align-items-center fs-13px text-gray-600">Email
                                Address</label>
                            @error('email')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-floating mb-15px">
                            <input name="password" type="password" class="form-control h-45px fs-13px"
                                placeholder="Password" id="password" />
                            <label for="password"
                                class="d-flex align-items-center fs-13px text-gray-600">Password</label>
                            @error('password')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-check mb-30px">
                            <input class="form-check-input" type="checkbox" value="1" id="rememberMe"
                                {{ old('remember') ? 'checked' : '' }} />
                            <label class="form-check-label" for="rememberMe">
                                Remember Me
                            </label>
                            @if (Route::has('password.request'))
                                {{-- <a href="{{ route('password.request') }}" class="text-primary">Forgot Password</a> --}}
                            @endif
                        </div>
                        <div class="mb-15px">
                            <button type="submit" class="btn btn-success d-block h-45px w-100 btn-lg fs-14px">Sign me
                                in</button>
                        </div>

                        <hr class="bg-gray-600 opacity-2" />

                    </form>

                </div>
                <!-- END login-content -->
            </div>
            <!-- END login-container -->
        </div>
        <!-- END login -->


    </div>
    <!-- END #app -->

    <!-- ================== BEGIN core-js ================== -->
    <script src="{{ asset('assets/js/vendor.min.js') }}"></script>
    <script src="{{ asset('assets/js/app.min.js') }}"></script>
    <script src="{{ asset('assets/js/theme/facebook.min.js') }}"></script>
    <!-- ================== END core-js ================== -->
</body>

</html>
