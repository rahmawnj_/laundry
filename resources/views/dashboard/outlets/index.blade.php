@php
    $title = 'Outlet';
@endphp
@extends('layouts.dashboard.app')
@section('title', $title ?? '')

@section('content')
    <!-- BEGIN breadcrumb -->
    {{-- <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
        <li class="breadcrumb-item"><a href="javascript:;">Page Options</a></li>
        <li class="breadcrumb-item active">Blank Page</li>
    </ol>
    <!-- END breadcrumb -->
    <!-- BEGIN page-header -->
    <h1 class="page-header">Blank Page <small>header small text goes here...</small></h1>
    <!-- END page-header -->
    <!-- BEGIN panel --> --}}
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">{{ $title ?? '' }}</h4>
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i
                        class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i
                        class="fa fa-redo"></i></a>
               
                @if (auth()->user()->hasRole('admin'))
                    <a href="{{ route('outlets.create') }}" class="btn btn-xs btn-primary">Add</a>
                @endif
            </div>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Logo</th>
                            <th>Name</th>
                            <th>City</th>
                            <th>Address</th>
                            <th>Code</th>
                            <th>Username</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($outlets as $outlet)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    @if ($outlet->logo)
                                        <img src="{{ asset($outlet->logo) }}" alt="User Image" class="img-thumbnail"
                                            style="max-width: 100px;">
                                    @else
                                        No Image
                                    @endif
                                </td>
                                <td>{{ $outlet->name }}</td>
                                <td>{{ $outlet->city }}</td>
                                <td>{{ $outlet->address }}</td>
                                <td>{{ $outlet->code }}</td>
                                <td>{{ $outlet->username }}</td>
                                <td>
                                    <a href="{{ route('outlets.edit', $outlet) }}" class="btn btn-primary btn-sm">Edit</a>
                                    <form action="{{ route('outlets.destroy', $outlet) }}" method="POST" class="d-inline">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger btn-sm"
                                            onclick="return confirm('Are you sure you want to delete this outlet?')">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <!-- END panel -->
@endsection
