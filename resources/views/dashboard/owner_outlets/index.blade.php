@php
    $title = 'Outlet'
@endphp
@extends('layouts.dashboard.app')
@section('title', $title ?? '')
@section('content')
    <!-- BEGIN breadcrumb -->
    {{-- <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
        <li class="breadcrumb-item"><a href="javascript:;">Page Options</a></li>
        <li class="breadcrumb-item active">Blank Page</li>
    </ol>
    <!-- END breadcrumb -->
    <!-- BEGIN page-header -->
    <h1 class="page-header">Blank Page <small>header small text goes here...</small></h1>
    <!-- END page-header -->
    <!-- BEGIN panel --> --}}
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">{{ $title ?? '' }}</h4>
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i
                        class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i
                        class="fa fa-redo"></i></a>
                
                <a href="{{ route('productcategories.create') }}" class="btn btn-xs btn-primary"><i
                        class="fa fa-plus"></i></a>
            </div>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Icon</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($productcategories as $productcategory)
                            <tr>
                                <td>{{ $productcategory->id }}</td>
                                <td>{{ $productcategory->name }}</td>
                                <td>
                                    @if ($productcategory->icon)
                                        {{ $productcategory->icon }}
                                    @else
                                        No Icon
                                    @endif
                                </td>

                                <td>
                                    <a href="{{ route('productcategories.edit', $productcategory->id) }}"
                                        class="btn btn-sm btn-primary">Edit</a>
                                    <form action="{{ route('productcategories.destroy', $productcategory->id) }}"
                                        method="POST" style="display: inline-block;">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-sm btn-danger"
                                            onclick="return confirm('Are you sure you want to delete this user?')">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">No productcategories found.</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END panel -->
@endsection
