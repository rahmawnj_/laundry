@php
    $title = 'Outlet'
@endphp
@extends('layouts.dashboard.app')
@section('title', $title ?? '')
@section('content')
    <!-- BEGIN breadcrumb -->
    {{-- <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
        <li class="breadcrumb-item"><a href="javascript:;">Page Options</a></li>
        <li class="breadcrumb-item active">Blank Page</li>
    </ol>
    <!-- END breadcrumb -->
    <!-- BEGIN page-header -->
    <h1 class="page-header">Blank Page <small>header small text goes here...</small></h1>
    <!-- END page-header -->
    <!-- BEGIN panel --> --}}
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">{{ $title ?? '' }}</h4>
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i
                        class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i
                        class="fa fa-redo"></i></a>
               
            </div>
        </div>
        <div class="panel-body">
            <form action="{{ route('owner_outlets.submit', $user->id) }}" method="POST">
                @csrf
                @method('PUT')

                <div class="mb-3">
                    <label for="user_id" class="form-label">User: {{ $user->name }}</label>
                </div>

                <div class="mb-3">
                    <label for="outlets" class="form-label">Select Outlets:</label>
                    @foreach ($outlets as $outlet)
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="outlets[]" value="{{ $outlet->id }}"
                                {{ in_array($outlet->id, $user->outlets->pluck('id')->toArray()) ? 'checked' : '' }}>
                            <label class="form-check-label">
                                {{ $outlet->name }}
                            </label>
                        </div>
                    @endforeach
                </div>

                <button type="submit" class="btn btn-primary">Update Outlets</button>
            </form>

        </div>
    </div>
    <!-- END panel -->
@endsection
